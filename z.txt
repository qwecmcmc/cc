#EXTM3U x-tvg-url="https://live.fanmingming.com/e.xml" catchup="append" catchup-source="?playseek=${(b)yyyyMMddHHmmss}-${(e)yyyyMMddHHmmss}"
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-1
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226016/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-2
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225588/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-3
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226021/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-4
http://[2409:8087:1a0a:df::4031]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226428/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-5
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226019/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-5+
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225603/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-6
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226010/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-7
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225733/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-8
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226008/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-9
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225734/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-10
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225730/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-11
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225597/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-12
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225731/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-13
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226011/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-14
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225732/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-15
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225601/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-16
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226100/index.m3u8
#EXTINF:-1 group-title="央视频道(IPV6)",CCTV-17
http://[2409:8087:1a0a:df::404b]:80/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225765/index.m3u8
#EXTINF:-1,tvg-id="东南卫视" tvg-name="东南卫视" tvg-logo="https://epg.v1.mk/logo/东南卫视.png" group-title="卫视频道(IPV4)",东南卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226341/index.m3u8
#EXTINF:-1,tvg-id="东方卫视" tvg-name="东方卫视" tvg-logo="https://epg.v1.mk/logo/东方卫视.png" group-title="卫视频道(IPV4)",东方卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225735/index.m3u8
#EXTINF:-1,tvg-id="东方卫视" tvg-name="东方卫视" tvg-logo="https://epg.v1.mk/logo/东方卫视.png" group-title="卫视频道(IPV4)",东方卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226345/index.m3u8
#EXTINF:-1,tvg-id="北京卫视" tvg-name="北京卫视" tvg-logo="https://epg.v1.mk/logo/北京卫视.png" group-title="卫视频道(IPV4)",北京卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225728/index.m3u8
#EXTINF:-1,tvg-id="北京卫视" tvg-name="北京卫视" tvg-logo="https://epg.v1.mk/logo/北京卫视.png" group-title="卫视频道(IPV4)",北京卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226450/index.m3u8
#EXTINF:-1,tvg-id="吉林卫视" tvg-name="吉林卫视" tvg-logo="https://epg.v1.mk/logo/吉林卫视.png" group-title="卫视频道(IPV4)",吉林卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226397/index.m3u8
#EXTINF:-1,tvg-id="四川卫视" tvg-name="四川卫视" tvg-logo="https://epg.v1.mk/logo/四川卫视.png" group-title="卫视频道(IPV4)",四川卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226338/index.m3u8
#EXTINF:-1,tvg-id="天津卫视" tvg-name="天津卫视" tvg-logo="https://epg.v1.mk/logo/天津卫视.png" group-title="卫视频道(IPV4)",天津卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225740/index.m3u8
#EXTINF:-1,tvg-id="天津卫视" tvg-name="天津卫视" tvg-logo="https://epg.v1.mk/logo/天津卫视.png" group-title="卫视频道(IPV4)",天津卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226459/index.m3u8
#EXTINF:-1,tvg-id="安徽卫视" tvg-name="安徽卫视" tvg-logo="https://epg.v1.mk/logo/安徽卫视.png" group-title="卫视频道(IPV4)",安徽卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226391/index.m3u8
#EXTINF:-1,tvg-id="山东卫视" tvg-name="山东卫视" tvg-logo="https://epg.v1.mk/logo/山东卫视.png" group-title="卫视频道(IPV4)",山东卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226456/index.m3u8
#EXTINF:-1,tvg-id="广东卫视" tvg-name="广东卫视" tvg-logo="https://epg.v1.mk/logo/广东卫视.png" group-title="卫视频道(IPV4)",广东卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226248/index.m3u8
#EXTINF:-1,tvg-id="江苏卫视" tvg-name="江苏卫视" tvg-logo="https://epg.v1.mk/logo/江苏卫视.png" group-title="卫视频道(IPV4)",江苏卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225613/index.m3u8
#EXTINF:-1,tvg-id="江苏卫视" tvg-name="江苏卫视" tvg-logo="https://epg.v1.mk/logo/江苏卫视.png" group-title="卫视频道(IPV4)",江苏卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226310/index.m3u8
#EXTINF:-1,tvg-id="江西卫视" tvg-name="江西卫视" tvg-logo="https://epg.v1.mk/logo/江西卫视.png" group-title="卫视频道(IPV4)",江西卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226344/index.m3u8
#EXTINF:-1,tvg-id="河北卫视" tvg-name="河北卫视" tvg-logo="https://epg.v1.mk/logo/河北卫视.png" group-title="卫视频道(IPV4)",河北卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226406/index.m3u8
#EXTINF:-1,tvg-id="河南卫视" tvg-name="河南卫视" tvg-logo="https://epg.v1.mk/logo/河南卫视.png" group-title="卫视频道(IPV4)",河南卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226480/index.m3u8
#EXTINF:-1,tvg-id="浙江卫视" tvg-name="浙江卫视" tvg-logo="https://epg.v1.mk/logo/浙江卫视.png" group-title="卫视频道(IPV4)",浙江卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225612/index.m3u8
#EXTINF:-1,tvg-id="浙江卫视" tvg-name="浙江卫视" tvg-logo="https://epg.v1.mk/logo/浙江卫视.png" group-title="卫视频道(IPV4)",浙江卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226339/index.m3u8
#EXTINF:-1,tvg-id="海南卫视" tvg-name="海南卫视" tvg-logo="https://epg.v1.mk/logo/海南卫视.png" group-title="卫视频道(IPV4)",海南卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226465/index.m3u8
#EXTINF:-1,tvg-id="深圳卫视" tvg-name="深圳卫视" tvg-logo="https://epg.v1.mk/logo/深圳卫视.png" group-title="卫视频道(IPV4)",深圳卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225739/index.m3u8
#EXTINF:-1,tvg-id="深圳卫视" tvg-name="深圳卫视" tvg-logo="https://epg.v1.mk/logo/深圳卫视.png" group-title="卫视频道(IPV4)",深圳卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226313/index.m3u8
#EXTINF:-1,tvg-id="湖北卫视" tvg-name="湖北卫视" tvg-logo="https://epg.v1.mk/logo/湖北卫视.png" group-title="卫视频道(IPV4)",湖北卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225627/index.m3u8
#EXTINF:-1,tvg-id="湖北卫视" tvg-name="湖北卫视" tvg-logo="https://epg.v1.mk/logo/湖北卫视.png" group-title="卫视频道(IPV4)",湖北卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226477/index.m3u8
#EXTINF:-1,tvg-id="湖南卫视" tvg-name="湖南卫视" tvg-logo="https://epg.v1.mk/logo/湖南卫视.png" group-title="卫视频道(IPV4)",湖南卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225610/index.m3u8
#EXTINF:-1,tvg-id="湖南卫视" tvg-name="湖南卫视" tvg-logo="https://epg.v1.mk/logo/湖南卫视.png" group-title="卫视频道(IPV4)",湖南卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226307/index.m3u8
#EXTINF:-1,tvg-id="贵州卫视" tvg-name="贵州卫视" tvg-logo="https://epg.v1.mk/logo/贵州卫视.png" group-title="卫视频道(IPV4)",贵州卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226474/index.m3u8
#EXTINF:-1,tvg-id="重庆卫视" tvg-name="重庆卫视" tvg-logo="https://epg.v1.mk/logo/重庆卫视.png" group-title="卫视频道(IPV4)",重庆卫视
http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226409/index.m3u8
#EXTINF:-1,tvg-id="安徽卫视" tvg-name="安徽卫视" tvg-logo="https://epg.v1.mk/logo/安徽卫视.png" group-title="卫视频道(IPV4)",安徽卫视
http://39.164.160.249:9901/tsfile/live/0130_1.m3u8
#EXTINF:-1,tvg-id="兵团卫视" tvg-name="兵团卫视" tvg-logo="https://epg.v1.mk/logo/兵团卫视.png" group-title="卫视频道(IPV4)",兵团卫视
http://219.159.194.129:8181/tsfile/live/0115_1.m3u8
#EXTINF:-1,tvg-id="兵团卫视" tvg-name="兵团卫视" tvg-logo="https://epg.v1.mk/logo/兵团卫视.png" group-title="卫视频道(IPV4)",兵团卫视
http://222.218.158.165:8181/tsfile/live/0115_1.m3u8
#EXTINF:-1,tvg-id="兵团卫视" tvg-name="兵团卫视" tvg-logo="https://epg.v1.mk/logo/兵团卫视.png" group-title="卫视频道(IPV4)",兵团卫视
http://112.101.78.50:9901/tsfile/live/0115_1.m3u8
#EXTINF:-1,tvg-id="广西卫视" tvg-name="广西卫视" tvg-logo="https://epg.v1.mk/logo/广西卫视.png" group-title="卫视频道(IPV4)",广西卫视
http://119.62.36.56:9901/tsfile/live/0136_1.m3u8
#EXTINF:-1,tvg-id="广西卫视" tvg-name="广西卫视" tvg-logo="https://epg.v1.mk/logo/广西卫视.png" group-title="卫视频道(IPV4)",广西卫视
https://tvbox.nat.netsite.cc:8043/proxy/102904669/102904669.m3u8
#EXTINF:-1,tvg-id="贵州卫视" tvg-name="贵州卫视" tvg-logo="https://epg.v1.mk/logo/贵州卫视.png" group-title="卫视频道(IPV4)",贵州卫视
http://221.213.69.82:9901/tsfile/live/0139_2.m3u8
#EXTINF:-1,tvg-id="海南卫视" tvg-name="海南卫视" tvg-logo="https://epg.v1.mk/logo/海南卫视.png" group-title="卫视频道(IPV4)",海南卫视
http://119.62.36.56:9901/tsfile/live/0112_1.m3u8
#EXTINF:-1,tvg-id="海南卫视" tvg-name="海南卫视" tvg-logo="https://epg.v1.mk/logo/海南卫视.png" group-title="卫视频道(IPV4)",海南卫视
https://tvbox.nat.netsite.cc:8043/proxy/2019508840/2019508840.m3u8
#EXTINF:-1,tvg-id="海峡卫视" tvg-name="海峡卫视" tvg-logo="https://epg.v1.mk/logo/海峡卫视.png" group-title="卫视频道(IPV4)",海峡卫视
http://38.64.72.148:80/hls/modn/list/4009/chunklist0.m3u8
#EXTINF:-1,tvg-id="海峡卫视" tvg-name="海峡卫视" tvg-logo="https://epg.v1.mk/logo/海峡卫视.png" group-title="卫视频道(IPV4)",海峡卫视
http://117.27.190.42:9998/tsfile/live/23283_1.m3u8
#EXTINF:-1,tvg-id="海峡卫视" tvg-name="海峡卫视" tvg-logo="https://epg.v1.mk/logo/海峡卫视.png" group-title="卫视频道(IPV4)",海峡卫视
http://117.27.190.42:9998/tsfile/live/23283_1.m3u8?key=txiptv&playlive=1&authid=0
#EXTINF:-1,tvg-id="海峡卫视" tvg-name="海峡卫视" tvg-logo="https://epg.v1.mk/logo/海峡卫视.png" group-title="卫视频道(IPV4)",海峡卫视
http://r.jdshipin.com/WtYt3
#EXTINF:-1,tvg-id="海峡卫视" tvg-name="海峡卫视" tvg-logo="https://epg.v1.mk/logo/海峡卫视.png" group-title="卫视频道(IPV4)",海峡卫视
http://117.27.190.42:9998/tsfile/live/23283_1.m3u8?key=txiptv
#EXTINF:-1,tvg-id="河北卫视" tvg-name="河北卫视" tvg-logo="https://epg.v1.mk/logo/河北卫视.png" group-title="卫视频道(IPV4)",河北卫视
http://39.164.160.249:9901/tsfile/live/0117_1.m3u8
#EXTINF:-1,tvg-id="河北卫视" tvg-name="河北卫视" tvg-logo="https://epg.v1.mk/logo/河北卫视.png" group-title="卫视频道(IPV4)",河北卫视
http://119.62.36.173:9901/tsfile/live/0118_1.m3u8
#EXTINF:-1,tvg-id="河南卫视" tvg-name="河南卫视" tvg-logo="https://epg.v1.mk/logo/河南卫视.png" group-title="卫视频道(IPV4)",河南卫视
http://d0bf2d.tv.netsite.cc/proxy/251465659/251465659.m3u8
#EXTINF:-1,tvg-id="河南卫视" tvg-name="河南卫视" tvg-logo="https://epg.v1.mk/logo/河南卫视.png" group-title="卫视频道(IPV4)",河南卫视
http://119.62.36.56:9901/tsfile/live/0125_1.m3u8
#EXTINF:-1,tvg-id="河南卫视" tvg-name="河南卫视" tvg-logo="https://epg.v1.mk/logo/河南卫视.png" group-title="卫视频道(IPV4)",河南卫视
https://tvbox.nat.netsite.cc:8043/proxy/251465659/251465659.m3u8
#EXTINF:-1,tvg-id="江苏卫视" tvg-name="江苏卫视" tvg-logo="https://epg.v1.mk/logo/江苏卫视.png" group-title="卫视频道(IPV4)",江苏卫视
http://119.62.36.56:9901/tsfile/live/0127_1.m3u8